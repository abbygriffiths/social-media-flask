from flask import (Flask, g, render_template, flash, redirect, url_for,)
from flask_bcrypt import check_password_hash
from flask_login import (LoginManager, login_user, logout_user,
                         login_required, current_user)

import os

import forms
import models


DEBUG = True
PORT = os.environ.get('PORT', 8000)
HOST = '0.0.0.0'

app = Flask(__name__)
app.secret_key = 'njksadblkoun98u8ubu@JBJHABDgiHJKAB@EhkjbcIU@*hlip][fjf7qy77t`'

login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'login'


@login_manager.user_loader
def load_user(user_id):
    try:
        return models.User.get(models.User.id==user_id)
    except models.DoesNotExist:
        return None


@app.before_request
def before_request():
    '''Connect to the DB before each request'''
    g.db = models.DATABASE
    g.db.connect()


@app.after_request
def after_request(response):
    '''Close the DB connection after each request'''
    g.db.close()
    return response


@app.route('/register', methods=('GET', 'POST'))
def register():
    form = forms.RegistrationForm()
    if form.validate_on_submit():
        flash('You successfully registered!', 'success')

        try:
            models.User.create_user(username=form.username.data,
                                    email=form.email.data,
                                    password=form.password.data)
            return redirect(url_for('index'))
        except ValueError:
            print("User already exists")
    return render_template('register.html', form=form)


@app.route('/login', methods=('GET', 'POST'))
def login():
    form = forms.LoginForm()

    if form.validate_on_submit():
        try:
            user = models.User.get(models.User.email == form.email.data)
        except models.DoesNotExist:
            flash('Your e-mail or password does not exist!', 'error')
        else:
            if check_password_hash(user.password, form.password.data):
                login_user(user)
                flash('You have been logged in!', 'success')
                return redirect(url_for('index'))
            else:
                flash('Your e-mail or password does not exist!', 'error')
    return render_template('login.html', form=form)


@app.route('/logout')
@login_required
def logout():
    logout_user()
    flash("You've been logged out! See you again soon!", 'success')
    return redirect(url_for('index'))


@app.route('/')
def index():
    return render_template('index.html')


if __name__ == '__main__':
    try:
        models.initialize()
        models.User.create_user(username='abirbhavg',
                                email='abirbhavg@icloud.com',
                                password='password',
                                admin=True)
    except ValueError:
        pass
    app.run(debug=DEBUG, host=HOST, port=PORT)
